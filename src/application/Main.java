package application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;


/*Multiplas Scenes
 *  - Uma Scene � o equivalente ao formul�rio em C#. Ter v�rios permite aumentar a complexidade dos nossos programas
 *  - passo 1 - declarar 2 scenes na zona dos atribuitos para ser visivel a todos os m�todos
 *  - passo 2 - alternar entre ambas , atrav�s de bot�es
 * */
public class Main extends Application {
	Scene scene1, scene2; //passo 1 - declarar duas scenes
//Cria Bot�o null, fora dos m�todos para ser acess�vel a todos
	@Override
	public void start(Stage primaryStage) {
		try {
			//scene 1
			Button btn1 = new Button("Saltar para a Scene 2");
			btn1.setOnAction(e->primaryStage.setScene(scene2));	
			StackPane layout1 = new StackPane(btn1);
			scene1 = new Scene(layout1,900,500);
			
			//scene 2
			Button btn2 = new Button("Saltar para a Scene1");
			btn2.setOnAction(e->primaryStage.setScene(scene1));	
			StackPane layout2 = new StackPane(btn2);
			scene2 = new Scene(layout2,800,400);
			
			
			//Na janela (Stage)
			primaryStage.setScene(scene1);				//define qual das Scenes entra 1�
			primaryStage.setTitle("SceneSwitch");		//T�tulo da Janela
			primaryStage.show();						//Executa
			
			
			
			
		} catch(Exception e) {					//Tratamento Gen�rico das Exe��es
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) {
		launch(args);
	}
}